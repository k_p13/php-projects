<?php
//database connection
$server = "localhost";
$username = "root";
$password = "";
$database = "notes";
$insert = false;
$update = false;
$delete = false;
//
$conn = mysqli_connect($server, $username, $password, $database);
if(!$conn){
  die("Sorry,  we couldn't connect to database". mysqli_connect_error());
}
if(isset($_GET['delete'])){
    $sno = $_GET['delete'];
    $delete = true;
    $sql = "DELETE FROM `notes` WHERE `id` = $sno";
    $result = mysqli_query($conn, $sql);
}
if($_SERVER['REQUEST_METHOD']=='POST'){
  if (isset($_POST['snoEdit'])){
      // Update the record
      $sno = $_POST["snoEdit"];
      $title = $_POST["titleEdit"];
      $description = $_POST["descriptionEdit"];
      //Sql query to be executed
      $sql = "UPDATE `notes` SET `title` = '$title' , `description` = '$description' WHERE `id` = $sno";
      $result = mysqli_query($conn, $sql);
      if($result){
        $update = true;
        //echo "We updated the record successfully";
      }
      else{
          echo "We could not update the record successfully";
      }
  }
  else{
    $title = $_POST["title"];
    $description = $_POST["description"];
    $sql = "INSERT INTO `notes` (`title`, `description`) VALUES ('$title', '$description')";
    $result = mysqli_query($conn, $sql);
    if($result){
      $insert = true;
    }else{
      echo "error inserting record :-> mysqli_error($conn)";
    }
  }
}
?>
<?php

if($insert){
  echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
  <strong>Success!</strong> Your note has been inserted successfully
  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
    <span aria-hidden='true'>×</span>
  </button>
</div>";
}

if($update){
  echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
  <strong>Success!</strong> Your note has been updated successfully
  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
    <span aria-hidden='true'>×</span>
  </button>
</div>";
}

if($delete){
  echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
  <strong>Success!</strong> Your note has been deleted successfully
  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
    <span aria-hidden='true'>×</span>
  </button>
</div>";
}

?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
      integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <title>PHP CRUD</title>
  </head>
  <body>
    <!-- Button trigger modal -->
    <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
      Edit Modal
    </button> -->

    <!-- Modal -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="editModalLabel">Edit this Note</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

            <form method="POST" action="mycrud.php">
              <div class="modal-body">
                <input type="hidden" name="snoEdit" id="snoEdit">
                <div class="mb-3">
                  <label for="title" class="form-label">Title</label>
                  <input type="text" class="form-control" id="titleEdit" name="titleEdit">
                </div>
                <div class="mb-3">
                  <label for="desc" class="form-label">Description</label>
                  <textarea name="descriptionEdit" id="descriptionEdit" class="form-control" rows="4" cols="80"></textarea>
                </div>
                <div class="modal-footer d-block mr-auto">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
              </div>
            </form>


        </div>
      </div>
    </div>
    <div class="container my-4">

      <h1>Welcome to php crud app!</h1>

      <form method="POST" action="mycrud.php">
        <div class="mb-3">
          <label for="title" class="form-label">Title</label>
          <input type="text" class="form-control" id="title" name="title">
        </div>
        <div class="mb-3">
          <label for="desc" class="form-label">Description</label>
          <textarea name="description" class="form-control" rows="4" cols="80"></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Add Note</button>
      </form>

    </div>

    <div class="container">
      <table class="table" id="myTable">
        <thead>
          <tr>
            <th scope="col">Sr. No.</th>
            <th scope="col">Title</th>
            <th scope="col">Description</th>
            <th scope="col">Actions</th>
          </tr>
        </thead>
        <tbody>
          <?php
              // Read Operation
              $sql = "SELECT * FROM `notes`";
              $result = mysqli_query($conn, $sql);
              $sno = 0;
              while($row=mysqli_fetch_assoc($result)){
                $sno = $sno + 1;
                echo "<tr>
                <th scope='row'>".$sno."</th>
                <td>".$row['title']."</td>
                <td>".$row['description']."</td>
                <td> <button class='edit btn btn-sm btn-primary' id=".$row['id'].">Edit</button>
                <button class='delete btn btn-sm btn-primary' id=d".$row['id'].">Delete</button> </td>
                <tr>";
            }
          ?>
        </tbody>
      </table>
    </div>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
      integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
      crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
      integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
      crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
      integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
      crossorigin="anonymous"></script>
    </script>
    <script>
      edits = document.getElementsByClassName('edit');
      Array.from(edits).forEach((element)=>{
        element.addEventListener('click',(e)=>{
          tr = e.target.parentNode.parentNode;
          title = tr.getElementsByTagName("td")[0].innerText;
          description = tr.getElementsByTagName("td")[1].innerText;
          //console.log(title, description);
          titleEdit.value = title;
          descriptionEdit.value = description;
          $('#editModal').modal('toggle');
          snoEdit.value = e.target.id;
          console.log(e.target.id);
        })
      })

      deletes = document.getElementsByClassName('delete');
      Array.from(deletes).forEach((element) => {
        element.addEventListener("click", (e) => {
          console.log("delete ");
          sno = e.target.id.substr(1,);
          console.log(e.target.id);

          if (confirm("Are you sure you want to delete this note!")) {
            console.log("yes");
            window.location = `mycrud.php?delete=${sno}`;
          }
          else {
            console.log("no");
          }
        })
      })
    </script>

    <hr>

  </body>
</html>
