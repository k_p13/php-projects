<?php
    $json_data = file_get_contents("https://pomber.github.io/covid19/timeseries.json");
    $phpdata = json_decode($json_data, true);
    foreach ($phpdata as $key => $value) {
      $daysCount = count($value) - 1;
      $dayCountPrev = $daysCount - 1;
    }

    $total_confirmed = 0;
    $total_recovered = 0;
    $total_deaths = 0;

    foreach ($phpdata as $key => $value) {
      $total_confirmed = $total_confirmed + $value[$daysCount]['confirmed'];
      $total_recovered = $total_recovered + $value[$daysCount]['recovered'];
      $total_deaths = $total_deaths + $value[$daysCount]['deaths'];
    }
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

    <!-- Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

    <!-- Font Awesome -->
    <script src="https://kit.fontawesome.com/996973c893.js" crossorigin="anonymous"></script>

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Baloo+Thambi+2:wght@400;500;600;700;800&display=swap" rel="stylesheet">
    <style>
      body{
        font-family: "Baloo Thambi 2", cursive;
      }
    </style>
  </head>
  <body>
    <div class="container-fluid text-center my-5">
      <h2> Covid19 Tracker </h2>
      <p> Live tracker for covid cases worldwide. </p>
    </div>
    <div class="container my-5">
        <div class="row text-center">
            <div class="col-4 text-warning">
                <h4>Confirmed</h4>
                <h5><?php echo $total_confirmed;?></h5>
            </div>
            <div class="col-4 text-success">
                <h4>Recovered</h4>
                <h5><?php echo $total_recovered;?></h5>
            </div>
            <div class="col-4 text-danger">
                <h4>Deaths</h4>
                <h5><?php echo $total_deaths;?></h5>
            </div>
        </div>
    </div>
    <div class="container-fluid">
      <table class="table table-striped">
        <thead>
          <tr>
            <th scope="col">Sr.No.</th>
            <th scope="col">Countries</th>
            <th scope="col">Confirmed Cases</th>
            <th scope="col">Recovered Cases</th>
            <th scope="col">Deceased</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $sno = 0;
          foreach ($phpdata as $key => $value) {
            $sno = $sno+1;
            ?>
            <tr>
              <th><?php echo $sno; ?></th>
              <td><?php echo $key; ?></td>
              <td><?php echo $value[$daysCount]['confirmed']; ?></td>
              <td><?php echo $value[$daysCount]['recovered']; ?></td>
              <td><?php echo $value[$daysCount]['deaths']; ?></td>
            </tr>
        <?php } ?>
        </tbody>
      </table>
    </div>
  </body>
</html>
